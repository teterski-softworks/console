package confmt

import (
	"bufio"

	"github.com/pkg/errors"
)

//Confirm is the default confirmation prompt used by the console.
func Confirm(readWriter bufio.ReadWriter, prompt string) (bool, error) {

	readWriter.WriteString(prompt)

	for {
		readWriter.WriteString("Y/N: ")
		input, err := readWriter.ReadString('\n')
		if err != nil {
			return false, errors.WithStack(err)
		}
		switch input {
		case "Y\n", "y\n":
			return true, nil
		case "N\n", "n\n":
			return false, nil
		default:
			readWriter.WriteString(prompt)
		}

	}

}

//GetString prompts the user for input and returns it as a string.
func GetString(readWriter bufio.ReadWriter, prompt string) (string, error) {
	readWriter.WriteString(prompt)
	input, err := readWriter.ReadString('\n')
	if err != nil {
		return "", errors.WithStack(err)
	}
	input = input[:len(input)-1]
	return input, nil
}
