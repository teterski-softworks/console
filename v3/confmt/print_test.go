package confmt_test

import (
	"bufio"
	"bytes"
	"fmt"
	"testing"

	"gitlab.com/teterski-softworks/console/v3/confmt"
	"gitlab.com/teterski-softworks/testinghelpers"
)

//TODO: This doesn't really test the format - Do we want to?

func Test_Fprintf(t *testing.T) {

	signature := fmt.Sprintf("\nconfmt.Test_Fprintf - ")

	for _, str := range testinghelpers.TestingTableStrings {
		buffer := bytes.Buffer{}
		writer := bufio.NewWriter(&buffer)
		i, err := confmt.Fprintf(*writer, str.Input)
		if err != nil {
			t.Errorf("%s %s", signature, err.Error())
		} else if i != len(str.Expected) {
			t.Errorf("%s incorrect amount of bytes written. Expected: %d Actual: %d", signature, len(str.Expected), i)
		}

		result := buffer.String()
		if result != str.Expected {
			t.Errorf("%s Expected: %s Actual: %s", signature, str.Expected, result)
		}
	}

}

func Test_Fprint(t *testing.T) {
	signature := fmt.Sprintf("\nconfmt.Test_Fprint - ")

	for _, str := range testinghelpers.TestingTableStrings {
		buffer := bytes.Buffer{}
		writer := bufio.NewWriter(&buffer)
		i, err := confmt.Fprint(*writer, str.Input)
		if err != nil {
			t.Errorf("%s %s", signature, err.Error())
		} else if i != len(str.Expected) {
			t.Errorf("%s incorrect amount of bytes written. Expected: %d Actual: %d", signature, len(str.Expected), i)
		}

		result := buffer.String()
		if result != str.Expected {
			t.Errorf("%s Expected: %s Actual: %s", signature, str.Expected, result)
		}
	}

}

func Test_Fprintln(t *testing.T) {
	signature := fmt.Sprintf("\nconfmt.Test_Fprintln - ")

	for _, str := range testinghelpers.TestingTableStrings {
		buffer := bytes.Buffer{}
		writer := bufio.NewWriter(&buffer)
		i, err := confmt.Fprintln(*writer, str.Input)
		if err != nil {
			t.Errorf("%s %s", signature, err.Error())
		} else if i != len(str.Expected)+1 {
			t.Errorf("%s incorrect amount of bytes written. Expected: %d Actual: %d", signature, len(str.Expected)+1, i)
		}

		result := buffer.String()
		if result != fmt.Sprintln(str.Expected) {
			t.Errorf("%s Expected: %s Actual: %s", signature, str.Expected, result)
		}
	}

}
