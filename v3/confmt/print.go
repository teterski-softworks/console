package confmt

import (
	"bufio"
	"fmt"

	"github.com/pkg/errors"
)

//Fprintf formats according to a format specifier and writes to w.
//It returns the number of bytes written and any write error encountered.
func Fprintf(w bufio.Writer, format string, a ...interface{}) (int, error) {
	str := fmt.Sprintf(format, a...)
	i, err := w.WriteString(str)
	if err != nil {
		return i, errors.WithStack(err)
	}
	if err := w.Flush(); err != nil {
		return i, errors.WithStack(err)
	}
	return i, nil
}

//Fprint formats using the default formats for its operands and writes to w.
//Spaces are added between operands when neither is a string.
//It returns the number of bytes written and any write error encountered.
func Fprint(w bufio.Writer, a ...interface{}) (int, error) {
	str := fmt.Sprint(a...)
	i, err := Fprintf(w, str)
	if err != nil {
		return i, errors.WithStack(err)
	}
	return i, nil
}

//Fprintln formats using the default formats for its operands and writes to w.
//Spaces are always added between operands and a newline is appended.
//It returns the number of bytes written and any write error encountered.
func Fprintln(w bufio.Writer, a ...interface{}) (int, error) {
	str := fmt.Sprintln(a...)
	i, err := Fprintf(w, str)
	if err != nil {
		return i, errors.WithStack(err)
	}

	return i, nil
}
