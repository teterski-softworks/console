package confmt_test

import (
	"bufio"
	"bytes"
	"fmt"
	"testing"

	"gitlab.com/teterski-softworks/console/v3/confmt"
)

//TODO!

func Test_Confirm(t *testing.T) {
	signature := fmt.Sprintf("\nconfmt.Test_Confirm - ")

	buffer := bytes.Buffer{}
	readWriter := bufio.NewReadWriter(nil, bufio.NewWriter(&buffer))

	var result bool
	var err error
	var test func() = func() {
		result, err = confmt.Confirm(*readWriter, "")
	}
	go test()

	buffer.WriteString("Ysdsd\n")

	if err != nil {
		t.Errorf("%s %s", signature, err.Error())
	}

	if result != true {
		t.Errorf("%s Expected: %v Actual: %v", signature, true, result)
	}
	//TODO: False case

}

func Test_GetString(t *testing.T) {
	//signature := fmt.Sprintf("\nconfmt.Test_GetString - ")
}
