package helpmenu

import (
	"fmt"

	"gitlab.com/teterski-softworks/console/v3/command"
)

type Helper interface {
	command.Commander
	//Adds a Commander's info to the Helper.
	Add(command.Commander)
	//Retrieves the underlining Commander that makes up the Helper.
	GetCommander() command.Commander
}

type Help struct {
	input []string
	menu  map[string]string
	command.Commander
}

//New returns a new Helper.
func New(inputs ...string) Helper {
	var h Help
	h.menu = map[string]string{}
	h.input = inputs

	function := func(argc int, argv []string) error {
		for k, v := range h.menu {
			fmt.Println(k)
			fmt.Println(v)
		}

		return nil
	}

	h.Commander = command.New("Shows the help menu.", h.input, function)

	return &h
}

//Adds a Commander's info to the Help menu.
func (h *Help) Add(c command.Commander) {
	str := ""
	for _, i := range c.Inputs() {
		str += i + ", "
	}
	h.menu[str[:len(str)-2]] = "\t- " + c.Description()
}

//Retrieves the underlining Commander that makes up the Help menu.
func (h *Help) GetCommander() command.Commander {
	return h.Commander
}
