module gitlab.com/teterski-softworks/console/v3

go 1.16

require (
	github.com/pkg/errors v0.9.1
	gitlab.com/teterski-softworks/testinghelpers v1.4.0
)
