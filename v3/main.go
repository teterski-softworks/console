package main

import (
	"log"
	"os"

	"gitlab.com/teterski-softworks/console/v3/command"
	"gitlab.com/teterski-softworks/console/v3/console"
)

//An example of using the console package

func main() {

	c := console.New()
	c.Prompt = "console test > "

	logger := log.New(os.Stdout, "", log.LstdFlags)

	var Quit = command.New(
		"Ends the program.",
		[]string{"q", "quit", "exit"},
		func(argc int, argv []string) error {
			logger.Printf("Program ended. %v", argv)
			os.Exit(0)
			return nil
		},
	)
	c.Add(Quit)

	err := c.Listen()
	log.Print(err)

}
