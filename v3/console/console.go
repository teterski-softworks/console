package console

import (
	"bufio"
	"os"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/teterski-softworks/console/v3/command"
	"gitlab.com/teterski-softworks/console/v3/confmt"
	"gitlab.com/teterski-softworks/console/v3/helpmenu"
)

//Console provides command line console functionality
type Console struct {
	commands      map[string]command.Commander
	HelpMenu      helpmenu.Helper
	InvalidCmdMsg string
	Prompt        string
	ReadWriter    *bufio.ReadWriter
}

//New returns a new Console
func New() Console {
	var c Console
	c.InvalidCmdMsg = "Invalid command. Type 'h' or 'help' for the help menu to see a list of valid commands."
	c.commands = map[string]command.Commander{}
	c.HelpMenu = helpmenu.New("h", "help")
	c.Add(c.HelpMenu.GetCommander())
	c.Prompt = "> "
	c.ReadWriter = bufio.NewReadWriter(bufio.NewReader(os.Stdin), bufio.NewWriter(os.Stdout))

	return c
}

//Listen makes the console listen for user input.
func (c Console) Listen() error {

	for {
		//Get user input
		if _, err := c.Printf(c.Prompt); err != nil {
			return errors.WithStack(err)
		}

		input, err := c.ReadWriter.ReadString('\n')
		if err != nil {
			return errors.WithStack(err)
		}
		input = strings.TrimSuffix(input, "\n")
		if input == "" {
			continue
		}
		argv := strings.Split(input, " ")
		argc := len(argv)

		//Check if a command has been called
		executed := false
		for _, cmd := range c.commands {
			if !inArray(argv[0], cmd.Inputs()) {
				continue
			}
			err := cmd.Execute(argc, argv)
			if err != nil {
				return err

			}

			executed = true
			break
		}
		//If no valid command was entered, tell the user it is invalid.
		if !executed {
			if _, err := c.Println(c.InvalidCmdMsg); err != nil {
				return errors.WithStack(err)
			}
		}
	}

}

//inArray checks if val is in the array.
func inArray(val string, array []string) bool {

	for _, in := range array {
		if val == in {
			return true
		}
	}

	return false
}

//Add adds a new command to the Console.
func (c *Console) Add(cmd command.Commander) {
	for _, input := range cmd.Inputs() {
		c.commands[input] = cmd
	}
	c.HelpMenu.Add(cmd)
}

//Printf formats according to a format specifier and writes to console output.
//It returns the number of bytes written and any write error encountered.
func (c *Console) Printf(format string, a ...interface{}) (int, error) {
	return confmt.Fprintf(*c.ReadWriter.Writer, format, a...)
}

//Print formats using the default formats for its operands and writes to console output.
//Spaces are added between operands when neither is a string.
//It returns the number of bytes written and any write error encountered.
func (c *Console) Print(a ...interface{}) (int, error) {
	return confmt.Fprint(*c.ReadWriter.Writer, a...)
}

//Println formats using the default formats for its operands and writes to console output.
//Spaces are always added between operands and a newline is appended.
//It returns the number of bytes written and any write error encountered.
func (c *Console) Println(a ...interface{}) (int, error) {
	return confmt.Fprintln(*c.ReadWriter.Writer, a...)
}
