package command

//Commander represents a command that can be used by a Console.
type Commander interface {
	//Returns the valid inputs that will call the Commander.
	Inputs() []string
	//Execute runs the Commander's function.
	//	argc - the number of arguments passed in.
	//	argv - an array of the arguments passed in.
	Execute(argc int, argv []string) error
	//Returns a description of the Commander.
	Description() string
}

//Command represents a command that can be used by a Console.
type Command struct {
	inputs      []string
	function    func(argc int, argv []string) error
	description string
}

//New returns a new Commander
func New(description string, inputs []string, function func(argc int, argv []string) error) Commander {
	c := Command{
		inputs:      inputs,
		function:    function,
		description: description,
	}
	return &c
}

//Returns the valid inputs that will call the Command.
func (c Command) Inputs() []string {
	return c.inputs
}

//Returns a description of the Command.
func (c Command) Description() string {
	return c.description
}

/*Execute runs the Command's function.

argc - the number of arguments passed in.

argv - an array of the arguments passed in.
*/
func (c Command) Execute(argc int, argv []string) error {

	if err := c.function(argc, argv); err != nil {
		return err
	}

	return nil
}
